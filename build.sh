#!/bin/sh

. $(find -iname include.sh)

build() {
    CORES=$(nproc)

    print "\n${GRN}Building ${P_INF}${DIR_NAME}${NORM} using ${P_INF}$CORES${NORM} CPU threads..."

    make -j${nproc-1} all

    if [ $? -ne 0 ]; then
        error_report "build failed!" 2
    fi
}

burn() {
    target_search

    printf "\n\n${GRN}Attempting to burn ${P_INF}${TARGET}${N_ULIN}${GRN}...${NORM}\n"

    sudo cdrdao write --speed 2 --driver generic-mmc-raw --eject "${TARGET}"
    sudo cdrdao write --speed 2 --eject "${TARGET}"

    if [ $? -ne 0 ]; then
        error_report "burn failed!" 200
    fi
}

check_if_installed() {
    EMU=$1
    command -v ${EMU} >/dev/null 2>&1 || {
        error_report "${P_INF}${EMU}${NORM} doesn't appear to be installed." 3
    }
}

clean() {
    printf "\n\n${P_INF}Cleaning build files...${NORM}\n"

    rm -f ./joengine/jo_engine/*.o
    rm -f ./cd/0.bin
    rm -f *.o
    rm -f ./*.bin
    rm -f ./*.elf
    rm -f ./*.map
    rm -f ./*.iso

    make clean
}

error_report() {
    MSG=$1
    CODE=${2-1} # Code 1 if unspecified

    print "\n\n${P_ERR}ERROR: \t${MSG}"
    print "\t${L_RED}Aborting!${NORM}"

    exit $CODE
}

target_search() {
    # If a target hasn't been specified...
    LEN=${#TARGET}
    if [ $LEN -eq 0 ]; then
        # Try to find the target automatically
        LEN=${#DISC_FORMAT}
        if [ $LEN -gt 0 ]; then
            print "Attempting to find a valid ${P_WRN}${DISC_FORMAT}${NORM} file..."
            TARGET=$(ls *.$DISC_FORMAT)
        else
            print "${P_INF}No target or format specifed.${NORM} Attempting to find a valid target..."
            DISC_FORMATS="cue iso mds img"

            for FORMAT in ${DISC_FORMATS}; do
                print "Searching for a ${P_INF}$FORMAT${NORM} in ${P_INF}${PWD}${NORM}"
                TARGET=$(ls *.$FORMAT)

                if [ -f "${TARGET}" ]; then
                    break
                fi
            done
        fi
    fi
    if [ -f "${TARGET}" ]; then
        print "${P_SCC}Found '${ULIN}${TARGET}${N_ULIN}'${NORM}"
    else
        error_report "No valid target could be found. Did the build fail?" 4
    fi
}




run_emu() {
    EMU=$1

    target_search

    print "\n\n${GRN}Attempting to run using ${P_INF}${EMU}${N_ULIN}${GRN}...${NORM}"

    check_if_installed $EMU

    case $EMU in
        yabause)
            yabause -a -i "$TARGET"
        ;;

        retroarch)
            LIBRETRO_CORE_DIR=$(cat ~/.config/retroarch/retroarch.cfg \
                | grep libretro_directory \
                | awk '/=/ {print $3}' \
                | tr -d \")
            YABAUSE_CORE="${LIBRETRO_CORE_DIR}/yabause_libretro.so"

            # /usr/bin/retroarch -L ${YABAUSE_CORE} ${PWD}/${TARGET}
            retroarch -L ~/.config/retroarch/cores/yabause_libretro.so game.cue
        ;;

        *)
            $EMU "$TARGET"
        ;;
    esac

    exit $?
}

BUILD=0
BURN=0
CLEAN=0
EMU="retroarch"
DISC_FORMAT="cue"
RUN=0
WIPE=0

while [ $# -gt 0 ]; do
    # If a directory is specified, run the script there.
    if [ -d $1 ]; then
        START_DIR=${PWD}
        cd $1
        shift
    else
        case $1 in
            -b | --build)
                BUILD=1
                shift
            ;;

            --burn | --write)
                BURN=1
                shift
            ;;

            -c | --clean)
                CLEAN=1
                shift
            ;;

            -e | --emulator)
                EMU="$2"
                RUN=1
                shift
                shift
            ;;

            -f | --format)
                # Trim a dot if the user added one
                DISC_FORMAT=$(printf "$2" | tr -d .)
                shift
            ;;

            -r | --run)
                RUN=1
                shift
            ;;

            -t | --target)
                TARGET="$2"
                shift
            ;;

            -w | --wipe)
                WIPE=1
                shift
            ;;

            -[bc][bc])
                CLEAN=1
                BUILD=1
                shift
            ;;

            -[br][br])
                BUILD=1
                RUN=1
                shift
            ;;

            -[bcr][bcr][bcr])
                CLEAN=1
                BUILD=1
                RUN=1
                shift
            ;;

            *)  shift;;
        esac
    fi
done

# Clear the terminal
if [ $WIPE -eq 1 ]; then
    clear
fi

# Delete previous build(s)
if [ $CLEAN -eq 1 ]; then
    clean
fi

# Build
if [ $BUILD -eq 1 ]; then
    build
fi

# Run
if [ $RUN -eq 1 ]; then
    run_emu $EMU
fi

# Burn
if [ $BURN -eq 1 ]; then
    burn
fi

cd $START_DIR

exit 0
