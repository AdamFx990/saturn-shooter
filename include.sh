#!/bin/sh

import_libs() {
    if [ -d include ]; then
        LIB_DIR="scripts"
    else
        LIB_DIR=$(find -name "scripts")
    fi
    LIBS=$(find ${LIB_DIR} -maxdepth 1 -type f | sort)
    for LIB in $LIBS; do
        . ${LIB}
    done
    printf "Found the following shell libraries: \n${L_YLW}${LIBS}\n${NORM}"
}

import_libs
