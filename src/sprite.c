/* FILE: sprite.c
 * DESC: Logic commonly used by structs that inherit t_sprite
 */

#include "shooter/sprite.h"
#include "shooter/point.h"
#include "shooter/camera.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

/* Draw the given sprite to scale */
void sprite_draw(const t_sprite sprite) {
  const t_camera* c = get_camera();
  jo_sprite_change_sprite_scale(sprite.scale * c->zoom);
  jo_sprite_draw3D(sprite.id, sprite.pos.x - c->pos.x, sprite.pos.y - c->pos.y, sprite.pos.z);
  jo_sprite_restore_sprite_scale();
}

/* Draw the sprite at the given angle and to scale */
void sprite_draw_rotate_f(const t_sprite_f sprite, const int angle) {
  const t_camera* c = get_camera();
  jo_sprite_change_sprite_scale(sprite.scale);
  jo_sprite_draw3D_and_rotate(sprite.id, sprite.pos.x - c->pos.x, sprite.pos.y - c->pos.y, 500,
                              angle);
  jo_sprite_restore_sprite_scale();
}

/* Draw the sprite at the given angle and to scale */
void sprite_draw_rotate(const t_sprite sprite, const int angle) {
  const t_camera* c = get_camera();
  jo_sprite_change_sprite_scale(sprite.scale);
  jo_sprite_draw3D_and_rotate(sprite.id, sprite.pos.x - c->pos.x, sprite.pos.y - c->pos.y, 500,
                              angle);
  jo_sprite_restore_sprite_scale();
}

t_rectangle sprite_get_hitbox(const t_sprite sprite) {
  return (t_rectangle){.top_left = sprite_get_vertex_top_left(sprite),
                       .bot_right = sprite_get_vertex_bot_right(sprite)};
}

/* Returns true if two sprites are intersecting. */
char sprite_intersecting(const t_sprite s1, const t_sprite s2) {
  // Sprite 1 size
  const jo_size s1s = sprite_scaled_size(s1);
  // Sprite 1 top-left position
  const jo_pos2D s1p = sprite_get_vertex_top_left(s1);
  // Sprite 2 size
  const jo_size s2s = sprite_scaled_size(s2);
  // Sprite 2 top-left position
  const jo_pos2D s2p = sprite_get_vertex_top_left(s2);

  return jo_square_intersect(s1p.x, s1p.y, s1s.width, s1s.height, s2p.x, s2p.y,
                             s2s.width, s2s.height);
}

/* Returns the point for the top-left corner of a scaled sprite */
jo_pos2D sprite_get_vertex_top_left(const t_sprite sprite) {
  const jo_size half_size = sprite_scaled_size_half(sprite);
  return (jo_pos2D){.x = sprite.pos.x - half_size.width,
                    .y = sprite.pos.y - half_size.height};
}

/* Returns the point for the bottom-right corner of a scaled sprite */
jo_pos2D sprite_get_vertex_bot_right(const t_sprite sprite) {
  const jo_size half_size = sprite_scaled_size_half(sprite);
  return (jo_pos2D){.x = sprite.pos.x + half_size.width,
                    .y = sprite.pos.y + half_size.height};
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */
