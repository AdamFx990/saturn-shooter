// projectile.c
#include "shooter/projectile.h"

#include "shooter/asteroid.h"
#include "shooter/camera.h"
#include "shooter/crosshair.h"
#include "shooter/enemy.h"
#include "shooter/player.h"
#include "shooter/point.h"
#include "shooter/score.h"
#include "shooter/ship.h"
#include "shooter/sprite.h"
#include "shooter/vector.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static void projectile_draw(jo_node *const proj_node);
static bool projectile_hit_solid(jo_node *ship, void *projectile);
static void projectile_think(jo_node *const proj_node);
static char projectile_handle_enemy_hit(jo_node *proj_node);
static char projectile_handle_asteroid_hit(jo_node *proj_node);

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

t_projectile *projectile_init(const int sprite_id, const unsigned char speed,
                              const float scale, const vector2_f pos,
                              const unsigned char dmg, const unsigned char player, jo_list* proj_list) {
  t_projectile *const p = jo_malloc(sizeof(t_projectile));
  t_player* players = get_players();

  p->angle = crosshair_get_angle(&players[player].crosshair);
  p->dir = crosshair_get_direction(&players[player].crosshair, &players[player].ship);
  p->speed = speed;
  p->dmg = dmg;
  p->sprite.id = sprite_id;
  p->sprite.pos.x = pos.x;
  p->sprite.pos.y = pos.y;
  p->sprite.scale = scale;
  p->proj_list = proj_list;

  return p;
}

void projectile_draw_list(jo_list *const proj_list) {
  jo_list_foreach(proj_list, projectile_draw);
}

void projectile_think_list(jo_list *const proj_list) {
  jo_list_foreach(proj_list, projectile_think);
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */

static void projectile_draw(jo_node *const proj_node) {
  const t_camera* c = get_camera();
  t_projectile* const projectile = (t_projectile *)proj_node->data.ptr;

  sprite_draw_rotate_f(projectile->sprite, projectile->angle);
}

static bool projectile_hit_solid(jo_node *const solid_node, void *const proj) {
  // projectile sprite
  const t_sprite_f psf = ((t_projectile *)proj)->sprite;
  const t_sprite ps =
      (t_sprite){.id = psf.id,
                 .pos = (jo_pos3D){.x = psf.pos.x, .y = psf.pos.y, .z = 500},
                 .scale = psf.scale};
  // ship sprite
  const t_sprite ss = ((t_solid *)((jo_node *)solid_node->data.ptr))->sprite;

  return sprite_intersecting(ss, ps);
}

static void projectile_move(t_projectile* projectile) {
  // TODO: Refactor into vector
  projectile->sprite.pos.x -=
      (projectile->dir.x * (float)projectile->speed);
  projectile->sprite.pos.y -=
      (projectile->dir.y * (float)projectile->speed);
}

static void projectile_think(jo_node *const proj_node) {
  t_projectile* const projectile = (t_projectile *)proj_node->data.ptr;

  if (projectile == JO_NULL)
    return;

  projectile_move(projectile);

  const jo_pos2D pos = (jo_pos2D){.x = projectile->sprite.pos.x,
                                  .y = projectile->sprite.pos.y};
  if (point_outside_screen_bounds(&pos)) {
    jo_list_remove(projectile->proj_list, proj_node);
    return;
  }

  if (projectile_handle_enemy_hit(proj_node))
    return;
  if (projectile_handle_asteroid_hit(proj_node))
    return;
}

static char projectile_handle_enemy_hit(jo_node *const proj_node) {
  t_projectile* const projectile = (t_projectile *)proj_node->data.ptr;

  jo_node *const enemy_node = jo_list_first_where_true(
      get_enemy_list(), projectile_hit_solid, projectile);

  if (enemy_node != JO_NULL) {
    // Remove the projectile
    jo_list_remove(projectile->proj_list, proj_node);
    t_ship *const enemy = (t_ship *)enemy_node->data.ptr;
    enemy->solid.health -= projectile->dmg;
    if (enemy->solid.health <= 0) {
      jo_list_remove(get_enemy_list(), enemy_node);
      set_score(get_score() + enemy->solid.reward);
    }

    return 1;
  }

  return 0;
}

static char projectile_handle_asteroid_hit(jo_node *const proj_node) {
  t_projectile* const projectile = (t_projectile *)proj_node->data.ptr;

  jo_node *const asteroid_node = jo_list_first_where_true(
      get_asteroid_list(), projectile_hit_solid, projectile);

  if (asteroid_node != JO_NULL) {
    // Remove the projectile
    jo_list_remove(projectile->proj_list, proj_node);
    t_asteroid *const asteroid = (t_asteroid *)asteroid_node->data.ptr;
    jo_free(projectile);

    asteroid->solid.health -= projectile->dmg;
    if (asteroid->solid.health <= 0) {
      jo_list_remove(get_asteroid_list(), asteroid_node);
      jo_free(asteroid_node);
      set_score(get_score() + asteroid->solid.reward);
    }

    return 1;
  }

  return 0;
}
