
/* FILE: sprite.c
 * DESC: Logic commonly used by structs that inherit t_sprite
 */

#include "shooter/background.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

/* Initalise level background with an image from the BG/ directory */
void background_init(const char* file_name) {
  jo_img bg;
  bg.data = ((void*)0);
  jo_tga_loader(&bg, "BG", "GL.TGA", JO_COLOR_Black);
  jo_set_background_sprite(&bg, 0, 0);
  jo_free_img(&bg);
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */
