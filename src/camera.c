/* FILE: camera.c
 * DESC: Logic commonly used by structs that inherit t_camera
 */

#include "shooter/camera.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static t_camera __camera;

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void camera_init() {

  __camera.pos.x = 0.0f;
  __camera.pos.y = 0.0f;
  __camera.zoom = 1.0f;
}

void camera_draw() {

}

void camera_input() {

}

void camera_think() {

}

void camera_move(float x, float y) {
  __camera.pos.x = x;
  __camera.pos.y = y;
}

const t_camera* get_camera() {
  return &__camera;
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */
