// main.c
#include "shooter/asteroid.h"
#include "shooter/background.h"
#include "shooter/camera.h"
#include "shooter/enemy.h"
#include "shooter/menu.h"
#include "shooter/player.h"
#include "shooter/point.h"
#include "shooter/score.h"
#include "shooter/state.h"

static char is_cd_playing = 0;

void load_level(void) {
  jo_audio_play_cd_track(2, 3, 1);
  menu_dispose();
  // background_init("GL.TGA");
  enemy_init();
  asteroid_init();
  weapon_audio_init();
}

void draw(void) {
  switch (get_state()) {
  case STATE_MAIN_MENU:
    if (is_cd_playing != 0) {
      is_cd_playing = 0;
    }
    menu_draw();
    break;

  default:
    if (!is_cd_playing) {
      load_level();
      is_cd_playing = 1;
    }

    enemy_draw();
    asteroid_draw();
    player_draw();
    score_draw();
#ifdef JO_DEBUG
#ifdef DEBUG_TIME
    jo_printf(0, 0, "time: %i", jo_get_ticks());
#endif // DEBUG_TIME
#ifdef DEBUG_MEM
    jo_printf(0, 1, "Mem usage:%d%%", jo_memory_usage_percent());
    jo_printf(0, 2, "fragmentation:%d%%", jo_memory_fragmentation());
#endif // DEBUG_MEM
#endif // JO_DEBUG
    break;
  }
}

void input(void) {
  switch (get_state()) {
  case STATE_MAIN_MENU:
    menu_input();
    break;

  default:
    player_input();
    break;
  }
}

void think(void) {
  switch (get_state()) {
  case STATE_MAIN_MENU:
    menu_think();
    break;

  case STATE_IN_GAME:
    player_think();
    asteroid_think();
    enemy_think();
    score_think();
    camera_think();
    break;

  case STATE_PAUSED:
    break;
  }
}

void jo_main(void) {
  jo_core_init(JO_COLOR_Black);
  jo_audio_init();
  camera_init();
  menu_init();
  score_init();

  jo_core_add_callback(input);
  jo_core_add_callback(think);
  jo_core_add_callback(draw);

  jo_core_run();
}
