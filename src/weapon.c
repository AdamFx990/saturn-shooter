// weapon.c

#include "shooter/weapon.h"

#include "shooter/enemy.h"
#include "shooter/point.h"
#include "shooter/score.h"
#include "shooter/ship.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

static jo_sound snd_reload;

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void weapon_audio_init() {
  if (jo_audio_load_pcm("RLOAD.PCM", JoSoundMono8Bit, &snd_reload)) {
    snd_reload.volume = 50;
    jo_printf(0, 0, "RLOAD.PCM Load successful");
  } else {
    jo_printf(0, 0, "RLOAD.PCM Load failed!");
  }
}

t_weapon *weapon_init(const int proj_sprite_id, const int speed) {
  t_weapon *const w = jo_malloc(sizeof(t_weapon));

  w->ammo = 8;
  w->ammo_state = AMMO_PARTIAL;
  w->capacity = 8;
  JO_ZERO(w->cooldown_end_time);
  w->fireable = WEAPON_READY;
  w->reload_time = 720;

  jo_list_init(&w->proj_list);

  w->proj_template = (t_projectile){
      .dmg = 10,
      .speed = speed,
      .sprite = (t_sprite_f){.id = proj_sprite_id} // sprite
  }; // proj_template
  w->proj_list.allocation_behaviour = JO_MALLOC_TRY_REUSE_BLOCK;

  return w;
}

char weapon_can_fire(t_weapon *const weapon) {
  // Weapon is on cooldown
  if (weapon_cooldown_remaining(weapon)) {
    return 0;
  }
  // Out of ammo
  if (weapon->ammo <= 0) {
    weapon->ammo = 0;
    weapon->ammo_state = AMMO_EMPTY;
    weapon->fireable = WEAPON_EMPTY;
    return 0;
  }
  // Weapon has shots left, but isn't full
  else if (weapon->ammo > 0 && weapon->ammo < weapon->capacity) {
    weapon->ammo_state = AMMO_PARTIAL;
    weapon->fireable = WEAPON_READY;
  }
  // Weapon is full of lovely ammo
  else if (weapon->ammo >= weapon->capacity) {
    weapon->ammo = weapon->capacity;
    weapon->ammo_state = AMMO_FULL;
    weapon->fireable = WEAPON_READY;
  }

  return 1; // Can fire
}

int weapon_cooldown_remaining(t_weapon *const weapon) {
  const int cooldown = (weapon->cooldown_end_time - jo_get_ticks());

  if (cooldown <= 0)
    return 0;

  return cooldown;
}

char weapon_reload(t_weapon *const weapon) {
  // Check if weapon can be reloaded
  if (weapon->ammo_state == AMMO_FULL)
    return 0;
  // Check if weapon is on cooldown
  if (weapon_cooldown_remaining(weapon))
    return -1;

  weapon->cooldown_end_time = (jo_get_ticks() + weapon->reload_time);
  weapon->fireable = WEAPON_RELOADING;
  weapon->ammo_state = AMMO_FULL;
  weapon->ammo = weapon->capacity;

  jo_audio_play_sound(&snd_reload);

  return 1;
}

void weapon_dispose() {
  jo_audio_free_pcm(&snd_reload);
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */
