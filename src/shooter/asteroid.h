// health.h

#ifndef SATURN_SHOOTER_ASTEROID_H

#include "solid.h"
#include <jo/jo.h>

/* ====== *
 * MACROS *
 * ====== */

#define ASTEROID_RESPAWN_DIFFICULTY 100

/* ======= *
 * STRUCTS *
 * ======= */

struct asteroid {
  t_solid solid;
  int angle;
};

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

typedef struct asteroid t_asteroid;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void asteroid_init();
void asteroid_draw();
void asteroid_think();
jo_list* get_asteroid_list();

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

/* jo_list compatible call for sprite_draw() */
__jo_force_inline void asteroid_draw_list(jo_node *const asteroid_node) {
  const t_asteroid* asteroid = (t_asteroid *)asteroid_node->data.ptr;
  sprite_draw_rotate(asteroid->solid.sprite, asteroid->angle);
}

#define SATURN_SHOOTER_ASTEROID_H
#endif // SATURN_SHOOTER_ASTEROID_H
