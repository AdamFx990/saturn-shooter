// maths.h
#ifndef SATURN_SHOOTER_MATHS_H

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

float maths_sqrt_float_fast(float);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define MATHS_SQUARE(X) ((X) * (X))

#define SATURN_SHOOTER_MATHS_H
#endif // SATURN_SHOOTER_MATHS_H
