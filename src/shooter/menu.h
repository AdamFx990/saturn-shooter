// menu.h

#ifndef SATURN_SHOOTER_MENU_H

#include "array.h"

#include <jo/jo.h>

/* ====== *
 * MACROS *
 * ====== */

/* ===== *
 * ENUMS *
 * ===== */

/* ======= *
 * STRUCTS *
 * ======= */

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

static t_array controller_ids;

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void menu_init();
void menu_draw();
void menu_input();
void menu_think();
void menu_dispose();

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_MENU_H
#endif // SATURN_SHOOTER_MENU_H
