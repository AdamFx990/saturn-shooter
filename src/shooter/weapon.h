// weapon.h

#ifndef SATURN_SHOOTER_WEAPON_H

#include "projectile.h"

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

typedef enum {
  AMMO_FULL,    // Weapon is full and can't be reloaded
  AMMO_PARTIAL, // Weapon is partially full and can be either fired or be reloaded
  AMMO_EMPTY    // Weapon needs to be reloaded
} weapon_ammo_state;

typedef enum {
  // Needs reloading
  WEAPON_EMPTY,
  // More time must pass before the next shot can be fired
  WEAPON_RECENTLY_FIRED,
  // Is reloading
  WEAPON_RELOADING,
  // Can be fired
  WEAPON_READY
} weapon_fireable;

typedef struct weapon {
  unsigned char ammo;
  weapon_ammo_state ammo_state;
  unsigned char capacity;
  unsigned int cooldown_end_time;      // When the next shot can be fired
  weapon_fireable fireable;
  jo_list proj_list;          // Shots fired by this weapon that are yet to hit anything
  t_projectile proj_template; // Template for projectiles fired from this weapon
  int reload_time;            // in ticks
} t_weapon;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void weapon_audio_init();
t_weapon *weapon_init(int projectile_sprite_id, int projectile_speed);
char weapon_can_fire(t_weapon *);
int weapon_cooldown_remaining(t_weapon *);
char weapon_reload(t_weapon *);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_WEAPON_H
#endif // SATURN_SHOOTER_WEAPON_H
