// enemy.h

#ifndef SATURN_SHOOTER_ENEMY_H

#include "ship.h"
#include <jo/jo.h>

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void enemy_init();
void enemy_draw();
void enemy_think();
jo_list* get_enemy_list();

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_ENEMY_H
#endif // SATURN_SHOOTER_ENEMY_H
