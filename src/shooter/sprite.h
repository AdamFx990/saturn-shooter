/* FILE: sprite.h
 * DESC: Struct with data common to every 2D sprite + common methods that extend
 *       jo_sprites.
 */

#ifndef SATURN_SHOOTER_SPRITE_H

#include "geometry.h"
#include "vector.h"
#include <jo/jo.h>

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

typedef struct {
  int id;
  jo_pos3D pos;
  float scale;
} t_sprite;

typedef struct {
  int id;
  vector2_f pos;
  float scale;
} t_sprite_f;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void sprite_draw(t_sprite);
void sprite_draw_rotate(t_sprite, int angle);
void sprite_draw_rotate_f(t_sprite_f, int angle);
t_rectangle sprite_get_hitbox(t_sprite);
char sprite_intersecting(t_sprite s1, t_sprite s2);
jo_pos2D sprite_get_vertex_top_left(t_sprite);
jo_pos2D sprite_get_vertex_bot_right(t_sprite);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

// Calculate the scaled length of 1 side of a sprite
#define SPRITE_SCALED_AXIS_LENGTH(A, SCALE) (Uint16)((float)(A) * (SCALE))

__jo_force_inline Uint16 sprite_scaled_height(const t_sprite sprite) {
  return SPRITE_SCALED_AXIS_LENGTH(__jo_sprite_def[sprite.id].height,
                                   sprite.scale);
}

__jo_force_inline Uint16 sprite_scaled_height_half(const t_sprite sprite) {
  return JO_DIV_BY_2(SPRITE_SCALED_AXIS_LENGTH(
      __jo_sprite_def[sprite.id].height, sprite.scale));
}

__jo_force_inline Uint16 sprite_scaled_width(const t_sprite sprite) {
  return SPRITE_SCALED_AXIS_LENGTH(__jo_sprite_def[sprite.id].width,
                                   sprite.scale);
}

__jo_force_inline Uint16 sprite_scaled_width_half(const t_sprite sprite) {
  return JO_DIV_BY_2(SPRITE_SCALED_AXIS_LENGTH(__jo_sprite_def[sprite.id].width,
                                               sprite.scale));
}

// Returns a jo_size with the size of the sprite as it is drawn on-screen
__jo_force_inline jo_size sprite_scaled_size(const t_sprite sprite) {
  return (jo_size){.width = sprite_scaled_width(sprite),
                   .height = sprite_scaled_height(sprite)};
}

// Returns a jo_size with half the size of the sprite as it is drawn on-screen
__jo_force_inline jo_size sprite_scaled_size_half(const t_sprite sprite) {
  return (jo_size){.width = sprite_scaled_width_half(sprite),
                   .height = sprite_scaled_height_half(sprite)};
}

#define SATURN_SHOOTER_SPRITE_H
#endif // SATURN_SHOOTER_SPRITE_H
