// projectile.h

#ifndef SATURN_SHOOTER_PROJECTILE_H

#include "sprite.h"
#include "vector.h"
#include <jo/jo.h>

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

typedef struct projectile {
  int angle;
  vector2_f dir;
  unsigned char speed;
  t_sprite_f sprite;
  unsigned char dmg;
  jo_list* proj_list;
} t_projectile;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

t_projectile *projectile_init(const int sprite_id, const unsigned char speed,
                              const float scale, const vector2_f pos,
                              const unsigned char dmg, const unsigned char player, jo_list* proj_list);
void projectile_draw_list(jo_list * const proj_list);
void projectile_think_list(jo_list * const proj_list);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_PROJECTILE_H
#endif // SATURN_SHOOTER_PROJECTILE_H
