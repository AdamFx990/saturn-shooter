// ship.h

#ifndef SATURN_SHOOTER_SHIP_H

#include "solid.h"
#include "weapon.h"
#include <jo/jo.h>

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

typedef struct ship {
  t_solid solid;
  t_weapon *laser;
} t_ship;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

t_ship ship_init(const int sprite_id, const unsigned char speed, const float scale, const jo_pos2D pos, const char health);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

/* jo_list compatible call for sprite_draw() */
__jo_force_inline void ship_draw_list(jo_node *const ship_node) {
  sprite_draw(((t_ship *)ship_node->data.ptr)->solid.sprite);
}

#define SATURN_SHOOTER_SHIP_H
#endif // SATURN_SHOOTER_SHIP_H
