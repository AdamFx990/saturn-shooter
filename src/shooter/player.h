// player.h

#ifndef SATURN_SHOOTER_PLAYER_H

#include "array.h"
#include "ship.h"
#include "crosshair.h"

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

struct __player {
  t_ship ship;
  t_crosshair crosshair;
  char controller_id;
};

typedef struct __player t_player;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void player_init(t_array * controller_ids, const unsigned char count);
void player_draw();
void player_input();
void player_think();
t_player* get_players();
unsigned char get_player_count();

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_PLAYER_H
#endif // SATURN_SHOOTER_PLAYER_H
