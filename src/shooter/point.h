/* FILE: point.h
 * DESC: Common functions & macros for co-ordinates that don't belong in
 * joengine
 */

#ifndef SATURN_SHOOTER_POINT_H

#include <jo/jo.h>

#include "geometry.h"
#include "camera.h"

// Returns true if point is right of the left edge of the screen
#define POINT_INSIDE_SCREEN_BOUNDS_LEFT(X) ((X) > (-JO_TV_WIDTH_2))
// Returns true if point is left of the right edge of the screen
#define POINT_INSIDE_SCREEN_BOUNDS_RIGHT(X) ((X) < JO_TV_WIDTH_2)
// Returns true if point is below the top edge of the screen
#define POINT_INSIDE_SCREEN_BOUNDS_TOP(Y) ((Y) > (-JO_TV_HEIGHT_2))
// Returns true if point is above the bottom edge of the screen
#define POINT_INSIDE_SCREEN_BOUNDS_BOTTOM(Y) ((Y) < JO_TV_HEIGHT_2)

// Returns true if point is left of the left edge of the screen
#define POINT_OUTSIDE_SCREEN_BOUNDS_LEFT(X) ((X) < (-JO_TV_WIDTH_2))
// Returns true if point is right of the right edge of the screen
#define POINT_OUTSIDE_SCREEN_BOUNDS_RIGHT(X) ((X) > JO_TV_WIDTH_2)
// Returns true if point is above the top edge of the screen
#define POINT_OUTSIDE_SCREEN_BOUNDS_TOP(Y) ((Y) < (-JO_TV_HEIGHT_2))
// Returns true if point is below of the bottom edge of the screen
#define POINT_OUTSIDE_SCREEN_BOUNDS_BOTTOM(Y) ((Y) > JO_TV_HEIGHT_2)

// Returns true if point is touching the left edge of the screen
#define POINT_TOUCHING_SCREEN_EDGE_LEFT(X) ((X) == (-JO_TV_WIDTH_2))
// Returns true if point is touching the right edge of the screen
#define POINT_TOUCHING_SCREEN_EDGE_RIGHT(X) ((X) == JO_TV_WIDTH_2)
// Returns true if point is touching the top edge of the screen
#define POINT_TOUCHING_SCREEN_EDGE_TOP(Y) ((Y) == (-JO_TV_HEIGHT_2))
// Returns true if point is touching the bottom edge of the screen
#define POINT_TOUCHING_SCREEN_EDGE_BOTTOM(Y) ((Y) == JO_TV_HEIGHT_2)

// Returns true if point is within (and not touching) the edges of the screen
static inline char point_inside_screen_bounds(const jo_pos2D *point) {
  const t_camera* c = get_camera();
  return (POINT_INSIDE_SCREEN_BOUNDS_LEFT(point->x - c->pos.x) ||
          POINT_INSIDE_SCREEN_BOUNDS_RIGHT(point->x - c->pos.x) ||
          POINT_INSIDE_SCREEN_BOUNDS_TOP(point->y - c->pos.y) ||
          POINT_INSIDE_SCREEN_BOUNDS_BOTTOM(point->y - c->pos.y));
}

// Returns true if point is outside (and not touching) the boundary of the
// screen
static inline char point_outside_screen_bounds(const jo_pos2D *point) {
  const t_camera* c = get_camera();
  return (POINT_OUTSIDE_SCREEN_BOUNDS_LEFT(point->x - c->pos.x) ||
          POINT_OUTSIDE_SCREEN_BOUNDS_RIGHT(point->x - c->pos.x) ||
          POINT_OUTSIDE_SCREEN_BOUNDS_TOP(point->y - c->pos.y) ||
          POINT_OUTSIDE_SCREEN_BOUNDS_BOTTOM(point->y - c->pos.y));
}

// Same as POINT_OUTSIDE_SCREEN_BOUNDS_LEFT, but adjusted for camera position
static inline char point_outside_screen_bounds_left(const int x) {
  const t_camera* c = get_camera();
  return POINT_OUTSIDE_SCREEN_BOUNDS_LEFT(x - c->pos.x);
}

// Same as POINT_OUTSIDE_SCREEN_BOUNDS_RIGHT, but adjusted for camera position
static inline char point_outside_screen_bounds_right(const int x) {
  const t_camera* c = get_camera();
  return POINT_OUTSIDE_SCREEN_BOUNDS_RIGHT(x - c->pos.x);
}

// Same as POINT_OUTSIDE_SCREEN_BOUNDS_TOP, but adjusted for camera position
static inline char point_outside_screen_bounds_top(const int y) {
  const t_camera* c = get_camera();
  return POINT_OUTSIDE_SCREEN_BOUNDS_TOP(y - c->pos.y);
}

// Same as POINT_OUTSIDE_SCREEN_BOUNDS_BOTTOM, but adjusted for camera position
static inline char point_outside_screen_bounds_bottom(const int y) {
  const t_camera* c = get_camera();
  return POINT_OUTSIDE_SCREEN_BOUNDS_BOTTOM(y - c->pos.y);
}

#define SATURN_SHOOTER_POINT_H
#endif // SATURN_SHOOTER_POINT_H
