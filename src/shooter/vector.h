// vector.h
#ifndef SATURN_SHOOTER_VECTOR_H

#include "maths.h"

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

typedef struct {
  char x;
  char y;
} vector2_8;

typedef struct {
  short x;
  short y;
} vector2_16;

typedef struct {
  int x;
  int y;
} vector2_32;

typedef vector2_32 vector2;

typedef struct {
  float x;
  float y;
} vector2_f32;
// TODO: Make into a union called vector2
typedef vector2_f32 vector2_f;

typedef struct {
  vector2_8;
  char z;
} vector3_8;

typedef struct {
  vector2_16;
  short z;
} vector3_16;

typedef struct {
  vector2_32;
  int z;
} vector3_32;

typedef struct {
  vector2_f32;
  float z;
} vector3_f32;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

vector2 vector2_normalise(vector2);
vector2_f vector2_f_normalise(vector2_f);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

inline vector2 vector2_init(const int x, const int y) {
  return (vector2){.x = x, .y = y};
}

inline vector2_f vector2_f_init(const float x, const float y) {
  return (vector2_f){.x = x, .y = y};
}

inline float vector2_length(const vector2 v) {
  return maths_sqrt_float_fast(MATHS_SQUARE((float)v.x) +
                               MATHS_SQUARE((float)v.y));
}

inline float vector2_f_length(const vector2_f v) {
  return maths_sqrt_float_fast(MATHS_SQUARE(v.x) + MATHS_SQUARE(v.y));
}

inline vector2 vector2_sub(const vector2 v1, const vector2 v2) {
  return (vector2){.x = (v1.x - v2.x), .y = (v1.y - v2.y)};
}

inline vector2_f vector2_f_sub(const vector2_f v1, const vector2_f v2) {
  return (vector2_f){.x = (v1.x - v2.x), .y = (v1.y - v2.y)};
}

#define SATURN_SHOOTER_VECTOR_H
#endif // SATURN_SHOOTER_VECTOR_H
