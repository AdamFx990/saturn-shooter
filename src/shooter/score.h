// score.h

#ifndef SATURN_SHOOTER_SCORE_H

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void score_init();
void score_draw();
void score_think();
int get_score();
void set_score(int s);
int get_high_score();

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_SCORE_H
#endif // SATURN_SHOOTER_SCORE_H
