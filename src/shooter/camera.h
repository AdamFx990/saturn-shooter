// camera.h

#ifndef SATURN_SHOOTER_CAMERA_H

#include "vector.h"
#include <jo/jo.h>

/* ================ *
 * TYPEDEFS/STRUCTS *
 * ================ */

struct __camera {
  vector2_f pos;
  float zoom;
};

typedef struct __camera t_camera;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void camera_init();
void camera_draw();
void camera_input();
void camera_think();
void camera_move(float x, float y);
const t_camera* get_camera();

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_CAMERA_H
#endif // SATURN_SHOOTER_CAMERA_H
