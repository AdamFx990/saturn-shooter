// health.h

#ifndef SATURN_SHOOTER_SOLID_H

#include "sprite.h"
#include <jo/jo.h>

/* ====== *
 * MACROS *
 * ====== */

/* ======= *
 * STRUCTS *
 * ======= */

struct solid {
  char health;
  unsigned char reward;
  unsigned char speed;
  unsigned char dmg;
  vector2_f dir;
  t_sprite sprite;
};

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

typedef struct solid t_solid;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

bool solid_intersects_solid(jo_node *solid1, void *solid2);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_SOLID_H
#endif // SATURN_SHOOTER_SOLID_H
