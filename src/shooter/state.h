// state.h
#ifndef SATURN_SHOOTER_STATE_H

#include <jo/jo.h>

/* ====== *
 * MACROS *
 * ====== */

/* ===== *
 * ENUMS *
 * ===== */

enum __state { STATE_MAIN_MENU, STATE_IN_GAME, STATE_PAUSED };

/* ======= *
 * STRUCTS *
 * ======= */

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

typedef enum __state t_state;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

t_state get_state();
void set_state(t_state s);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_STATE_H
#endif // SATURN_SHOOTER_STATE_H
