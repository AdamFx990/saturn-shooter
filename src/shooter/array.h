// array.h
#ifndef SATURN_SHOOTER_ARRAY_H

#include <stddef.h>

/* ====== *
 * MACROS *
 * ====== */

// Returns the number of elements in the given array
#define ARRAY_LENGTH(x) ((int)(sizeof(x) / sizeof(x)[0]))

/* ======= *
 * STRUCTS *
 * ======= */

typedef struct {
  int *array;
  unsigned char used;
  unsigned char size;
} t_array;

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void array_init(t_array *a, const unsigned char initialSize);
void array_free(t_array *a);
void array_push(t_array *a, const int element);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_ARRAY_H
#endif // SATURN_SHOOTER_ARRAY_H
