// crosshair.h
#ifndef SATURN_SHOOTER_CROSSHAIR_H

#include "sprite.h"
#include "vector.h"
#include "ship.h"

/* ====== *
 * MACROS *
 * ====== */

#define AIM_ANGLE 65
#define ANGLE_OFFSET -90

/* ======= *
 * STRUCTS *
 * ======= */

struct __crosshair {
  int angle;
  t_sprite sprite;
};

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

typedef struct __crosshair t_crosshair;

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

t_crosshair crosshair_init(unsigned char player);
void crosshair_draw(t_crosshair *crosshair);
void crosshair_think(t_crosshair *crosshair, t_ship* ship);

int crosshair_get_angle(t_crosshair *crosshair);
vector2_f crosshair_get_direction(t_crosshair *crosshair, t_ship *ship);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_CROSSHAIR_H
#endif // SATURN_SHOOTER_CROSSHAIR_H
