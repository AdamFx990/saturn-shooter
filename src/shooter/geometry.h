/* FILE: geometry.h
 * DESC: Geometry logic
 */

#ifndef SATURN_SHOOTER_GEOMETRY_H

#include <jo/jo.h>

typedef struct {
  jo_pos2D top_left;  // Smallest X & Y
  jo_pos2D bot_right; // Largest  X & Y
} t_rectangle;

#define SATURN_SHOOTER_GEOMETRY_H
#endif // SATURN_SHOOTER_GEOMETRY_H
