// background.h
#ifndef SATURN_SHOOTER_BACKGROUND_H

#include <jo/jo.h>

/* ====== *
 * MACROS *
 * ====== */

/* ======= *
 * STRUCTS *
 * ======= */

/* ====== *
 * UNIONS *
 * ====== */

/* ======== *
 * TYPEDEFS *
 * ======== */

/* ================ *
 * GLOBAL VARIABLES *
 * ================ */

/* ================= *
 * GLOBAL PROTOTYPES *
 * ================= */

void background_init(const char* file_name);

/* ================ *
 * INLINE FUNCTIONS *
 * ================ */

#define SATURN_SHOOTER_BACKGROUND_H
#endif // SATURN_SHOOTER_BACKGROUND_H
