// state.c

#include "shooter/state.h"

static t_state state;

t_state get_state() {
  return state;
}

void set_state(t_state s) {
  state = s;
}
