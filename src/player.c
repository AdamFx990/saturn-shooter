// player.c

#include "shooter/player.h"
#include "shooter/array.h"
#include "shooter/asteroid.h"
#include "shooter/camera.h"
#include "shooter/enemy.h"
#include "shooter/point.h"
#include "shooter/score.h"
#include "shooter/solid.h"
#include "shooter/state.h"
#include "shooter/vector.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

static jo_sound snd_boom;
static jo_sound snd_pew;
static unsigned char player_count;
static t_player *players;

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static char player_is_colliding_with(jo_list *const list, jo_node *const node);
static char player_is_colliding_with_enemy(const unsigned char player);
static char player_is_colliding_with_asteroid(const unsigned char player);
static char player_shoot(const unsigned char i);

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void player_init(t_array *controller_ids, const unsigned char count) {
  player_count = count;
  // Allocate memory for the number of players selected in the menu
  players = malloc(player_count * sizeof(t_player));

  const int s_ship =
      jo_sprite_add_tga(JO_ROOT_DIR, "SHIP3.TGA", JO_COLOR_Black);
  const int s_blast =
      jo_sprite_add_tga(JO_ROOT_DIR, "BLAST.TGA", JO_COLOR_Blue);

  for (unsigned char i = 0; i < player_count; i++) {
    players[i].ship = ship_init(s_ship,
                                3,                          // speed
                                0.5f,                       // scale
                                (jo_pos2D){.x = 0, .y = 0}, // pos
                                100);                       // health
    players[i].ship.laser = weapon_init(s_blast, 4);
    players[i].crosshair = crosshair_init(i);
    // Assign the next available controller
    players[i].controller_id = controller_ids->array[i];
  }
  if (jo_audio_load_pcm("PEW0.PCM", JoSoundMono8Bit, &snd_pew)) {
    snd_pew.volume = 90;
  } else {
#ifdef JO_DEBUG
    jo_printf(0, 1, "PEW0.PCM Load failed!");
#endif // JO_DEBUG
  }
  if (jo_audio_load_pcm("BOOM.PCM", JoSoundMono8Bit, &snd_boom)) {
    snd_boom.volume = 80;
  } else {
#ifdef JO_DEBUG
    jo_printf(0, 0, "BOOM.PCM Load failed!");
#endif // JO_DEBUG
  }
}

void player_draw() {
  if (get_state() == STATE_PAUSED) {
    jo_printf(17, 14, "Paused");
  }

  for (unsigned char i = 0; i < player_count; i++) {
    sprite_draw(players[i].ship.solid.sprite);

    // scale projectiles with the player ship
    projectile_draw_list(&players[i].ship.laser->proj_list);

    // can fire also calculates the weapon states, so it must be run first
    weapon_can_fire(players[i].ship.laser);
    jo_printf(18 * i, 28, "> PLAYER %i <", i + 1);

    switch (players[i].ship.laser->fireable) {
    case WEAPON_RELOADING:
      jo_printf(18 * i, 29, "Reloading: %i  ",
                weapon_cooldown_remaining(players[i].ship.laser));
      break;

    case WEAPON_EMPTY:
      jo_printf(18 * i, 29, "Reload required!");
      break;

    case WEAPON_RECENTLY_FIRED:
      jo_printf(18 * i, 29, "Recently fired!");
      break;

    case WEAPON_READY:
      jo_printf(18 * i, 29, "Ammo: %i       ", players[i].ship.laser->ammo);
      break;
    }
    jo_printf(18 * i, 30, "Armour: %i   ", players[i].ship.solid.health);
    crosshair_draw(&players[i].crosshair);
  }
}

void player_input() {
  const t_camera *c = get_camera();
  for (unsigned char i = 0; i < player_count; i++) {
    if (jo_is_input_key_down(players[i].controller_id, JO_KEY_START)) {
      switch (get_state()) {
      case STATE_PAUSED:
        set_state(STATE_IN_GAME);
        break;

      case STATE_IN_GAME:
        set_state(STATE_PAUSED);
        continue;

      default:
        break;
      }
    }
    if (get_state() == STATE_PAUSED) {
      continue;
    }
    t_rectangle hitbox = sprite_get_hitbox(players[i].ship.solid.sprite);

    if (jo_is_input_key_pressed(players[i].controller_id, JO_KEY_LEFT) &&
        POINT_INSIDE_SCREEN_BOUNDS_LEFT(hitbox.top_left.x - c->pos.x)) {
      players[i].ship.solid.sprite.pos.x -= players[i].ship.solid.speed;
    }
    if (jo_is_input_key_pressed(players[i].controller_id, JO_KEY_RIGHT) &&
        POINT_INSIDE_SCREEN_BOUNDS_RIGHT(hitbox.bot_right.x - c->pos.x)) {
      players[i].ship.solid.sprite.pos.x += players[i].ship.solid.speed;
    }
    if (jo_is_input_key_pressed(players[i].controller_id, JO_KEY_UP) &&
        POINT_INSIDE_SCREEN_BOUNDS_TOP(hitbox.top_left.y - c->pos.y)) {
      players[i].ship.solid.sprite.pos.y -= players[i].ship.solid.speed;
    }
    if (jo_is_input_key_pressed(players[i].controller_id, JO_KEY_DOWN) &&
        POINT_INSIDE_SCREEN_BOUNDS_BOTTOM(hitbox.bot_right.y - c->pos.y)) {
      players[i].ship.solid.sprite.pos.y += players[i].ship.solid.speed;
    }
    if (jo_is_input_key_down(players[i].controller_id, JO_KEY_A)) {
      if (players[i].ship.laser->fireable == WEAPON_EMPTY) {
        weapon_reload(players[i].ship.laser);
      } else {
        player_shoot(i);
      }
    }
    if (jo_is_input_key_down(players[i].controller_id, JO_KEY_B)) {
      weapon_reload(players[i].ship.laser);
    }
    // crosshair input
    static const char rotation_speed = 3;
    if (jo_is_input_key_pressed(players[i].controller_id, JO_KEY_L))
      players[i].crosshair.angle -= rotation_speed;
    if (jo_is_input_key_pressed(players[i].controller_id, JO_KEY_R))
      players[i].crosshair.angle += rotation_speed;
  }

  // Make camera follow player 1
  camera_move(players[0].ship.solid.sprite.pos.x,
              players[0].ship.solid.sprite.pos.y);
}

void player_think() {
  for (unsigned char i = 0; i < player_count; i++) {
    player_is_colliding_with_asteroid(i);
    player_is_colliding_with_enemy(i);
    if (players[i].ship.solid.health < 1) {
      set_score(0);
      if (players[i].controller_id > 5) {
        snd_boom.pan = 127;
      } else {
        snd_boom.pan = -128;
      }
      // Player dies
      jo_audio_play_sound(&snd_boom);
      set_state(STATE_MAIN_MENU);
      menu_init();
      jo_free(&snd_boom);
      jo_free(&snd_boom);
    }
    projectile_think_list(&players[i].ship.laser->proj_list);
    crosshair_think(&players[i].crosshair, &players[i].ship);
  }
}

t_player *get_players() { return players; }

unsigned char get_player_count() { return player_count; }

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */

static char player_is_colliding_with(jo_list *const list, jo_node *const node) {
  if (node == JO_NULL) {
    return 0; // Player isn't intersecting any enemy ships
  }

  jo_list_remove(list, node);
  jo_free(node);

  return 1;
}

/* Returns true if player's bounding box is
 * intersecting an enemy ship's bounding box. */
static char player_is_colliding_with_enemy(const unsigned char player) {
  jo_node *const enemy = jo_list_first_where_true(
      get_enemy_list(), solid_intersects_solid, &players[player]);

  if (player_is_colliding_with(get_enemy_list(), enemy)) {
    players[player].ship.solid.health -= ((t_solid *)enemy->data.ptr)->dmg;
    return 1;
  }

  return 0;
}

/* Returns true if player's bounding box is
 * intersecting an asteroid's bounding box. */
static char player_is_colliding_with_asteroid(const unsigned char player) {
  jo_node *const asteroid = jo_list_first_where_true(
      get_asteroid_list(), solid_intersects_solid, &players[player]);

  if (player_is_colliding_with(get_asteroid_list(), asteroid)) {
    players[player].ship.solid.health -= ((t_solid *)asteroid->data.ptr)->dmg;
    return 1;
  }
  return 0;
}

/* Player presses the fire button.  */
static char player_shoot(const unsigned char i) {
  // Check if the weapon can be fired
  if (!weapon_can_fire(players[i].ship.laser)) {
    return 0;
  }

  if (players[i].controller_id > 5) {
    snd_pew.pan = 127;
  } else {
    snd_pew.pan = -128;
  }
  jo_audio_play_sound(&snd_pew);

  t_projectile *const p = projectile_init(
      players[i].ship.laser->proj_template.sprite.id, // sprite_id
      players[i].ship.laser->proj_template.speed,     // speed
      0.5f,                                           // scale
      vector2_f_init(players[i].ship.solid.sprite.pos.x,
                     players[i].ship.solid.sprite.pos.y), // pos
      10,                                                 // dmg
      i,                                                  // player
      &players[i].ship.laser->proj_list);                 // projectile list
  jo_list_data data = {.ptr = p};

  jo_list_add(&players[i].ship.laser->proj_list, data);

  players[i].ship.laser->ammo -= 1;

  return 1;
}
