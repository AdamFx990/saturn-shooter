// maths.c
#include "shooter/maths.h"

float maths_sqrt_float_fast(const float f) {
  unsigned int i = *(unsigned int *)(void *)&f;
  i += 127 << 23;
  i >>= 1;
  return *(float *)(void *)&i;
}
