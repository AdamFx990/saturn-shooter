// solid.c

#include "shooter/solid.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

/* Returns true if two solids' hitboxes are intersecting */
bool solid_intersects_solid(jo_node *const solid1, void *const solid2) {
  const t_sprite s1 = ((t_solid *)(jo_node *)solid1->data.ptr)->sprite;
  const t_sprite s2 = ((t_solid *)solid2)->sprite;

  return sprite_intersecting(s1, s2);
}
