// asteroid.c

#include "shooter/asteroid.h"
#include "shooter/point.h"
#include "shooter/score.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

static unsigned char asteroid_max;
static int asteroid_sprite_id;
static jo_list asteroid_list;

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static void asteroid_move();
static void asteroid_respawn();
static void asteroid_spawn();

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void asteroid_init() {
  asteroid_max = 5;
  if (asteroid_sprite_id != -1) {
    asteroid_sprite_id =
      jo_sprite_add_tga(JO_ROOT_DIR, "AROID.TGA", JO_COLOR_Green);
  }
  jo_list_init(&asteroid_list);
  asteroid_list.allocation_behaviour = JO_MALLOC_TRY_REUSE_BLOCK;
  asteroid_respawn();
}

void asteroid_draw() {
  jo_list_foreach(&asteroid_list, asteroid_draw_list);
#ifdef JO_DEBUG
#ifdef DEBUG_ASTEROID
  jo_printf(18, 10, "aroid list count=%i", asteroid_list.count);
#endif // DEBUG_ASTEROID
#endif // JO_DEBUG
}

void asteroid_think() {
  jo_list_foreach(&asteroid_list, asteroid_move);
  asteroid_respawn();
}

jo_list* get_asteroid_list() {
  return &asteroid_list;
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */

static void asteroid_move(jo_node *asteroid_node) {
  t_asteroid *const asteroid = (t_asteroid *)asteroid_node->data.ptr;
  asteroid->solid.sprite.pos.x += asteroid->solid.speed;
  asteroid->angle++;

  if (point_outside_screen_bounds_right(asteroid->solid.sprite.pos.x)) {
    jo_list_remove(&asteroid_list, asteroid_node);
    jo_free(asteroid);
  }
}

static void asteroid_respawn() {
  asteroid_max = 1 + ((get_score() || 1) / 1000);
  if (asteroid_list.count != asteroid_max &&
      jo_random(ASTEROID_RESPAWN_DIFFICULTY) == 1) {
    asteroid_spawn();
  }
}

static void asteroid_spawn() {
  const t_camera* c = get_camera();
  t_asteroid *const asteroid = jo_malloc(sizeof(t_asteroid));

  const unsigned char speed = jo_random(3);
  const unsigned char base_dmg = 70;
  const unsigned char speed_mod = 20;
  const unsigned char dmg = base_dmg + (speed_mod * speed);
  asteroid->solid.sprite.pos.x = -(JO_TV_WIDTH_2 + 64) + c->pos.x;
  asteroid->solid.sprite.pos.y = -(jo_random(JO_TV_HEIGHT) - JO_TV_HEIGHT_2) + c->pos.y;
  asteroid->solid.sprite.pos.z = 520;
  asteroid->solid.sprite.id = asteroid_sprite_id;
  asteroid->solid.sprite.scale = 0.5f;
  asteroid->solid.speed = speed;
  asteroid->solid.health = 50;
  asteroid->solid.reward = 10;
  asteroid->solid.dmg = dmg;
  asteroid->angle = 0;

  jo_list_data data = {.ptr = asteroid};
  jo_list_add(&asteroid_list, data);
}
