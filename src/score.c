// score.c

#include "shooter/score.h"

#include <jo/jo.h>

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static int __score;
static int __high_score;

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

inline void score_init() {
  JO_ZERO(__score);
  JO_ZERO(__high_score);
}

void score_draw() {
  static const char score_pos_x = 24;
  static const char score_first_line = 1;
  jo_printf(score_pos_x, score_first_line, "Score: %i", __score);
  jo_printf(score_pos_x, (score_first_line + 1), "High score: %i", __high_score);
}

void score_think() {
  if (__score > __high_score)
    __high_score = __score;
}

int get_score() {
  return __score;
}

void set_score(int s) {
  __score = s;
}

int get_high_score() {
  return __high_score;
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */
