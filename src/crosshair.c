// crosshair.c

#include "shooter/crosshair.h"
#include "shooter/player.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

static const int max_angle = (ANGLE_OFFSET + AIM_ANGLE);
static const int min_angle = (ANGLE_OFFSET - AIM_ANGLE);

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static void crosshair_calc_pos();

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

t_crosshair crosshair_init(unsigned char player) {
  t_crosshair crosshair;
  crosshair.angle = ANGLE_OFFSET;
  char fn[8];
  sprintf(fn, "CH%i.TGA", player);
  crosshair.sprite.id = jo_sprite_add_tga(JO_ROOT_DIR, fn, JO_COLOR_Black);
  crosshair.sprite.pos.z = 100;
  crosshair.sprite.scale = 1.0f;
  return crosshair;
}

void crosshair_draw(t_crosshair *crosshair) {
  jo_sprite_enable_half_transparency();
  sprite_draw(crosshair->sprite);
  jo_sprite_disable_half_transparency();
}

void crosshair_think(t_crosshair *const crosshair,
                     t_ship *const ship) {
  crosshair_calc_pos(crosshair, ship);

  if (crosshair->angle > max_angle)
    crosshair->angle = max_angle;
  else if (crosshair->angle < min_angle)
    crosshair->angle = min_angle;
}

int crosshair_get_angle(t_crosshair *crosshair) {
  return (crosshair->angle - ANGLE_OFFSET);
}

vector2_f crosshair_get_direction(t_crosshair *crosshair, t_ship *ship) {
  // TODO: refactor into vector
  return vector2_f_normalise(vector2_f_sub(
      vector2_f_init(ship->solid.sprite.pos.x,
                     ship->solid.sprite.pos.y), // Start
      vector2_f_init(crosshair->sprite.pos.x, crosshair->sprite.pos.y))); // End
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */

static void crosshair_calc_pos(t_crosshair *crosshair, t_ship *ship) {
  static const float r = 66.6f;
  crosshair->sprite.pos.x =
      ship->solid.sprite.pos.x + (r * jo_cosf(crosshair->angle));
  crosshair->sprite.pos.y =
      ship->solid.sprite.pos.y + (r * jo_sinf(crosshair->angle));
}
