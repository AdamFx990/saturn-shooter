// array.c
#include "shooter/array.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void array_init(t_array *a, const unsigned char initialSize) {
  a->array = malloc(initialSize * sizeof(int));
  a->used = 0;
  a->size = initialSize;
}

void array_push(t_array *a, int element) {
  // a->used is the number of used entries, because a->array[a->used++] updates a->used only *after* the array has been accessed.
  // Therefore a->used can go up to a->size
  if (a->used == a->size) {
    a->size *= 2;
    a->array = realloc(a->array, a->size * sizeof(int));
  }
  a->array[a->used++] = element;
}


void array_free(t_array *a) {
  jo_free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}
