// ship.c

#include "shooter/ship.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

t_ship ship_init(const int sprite_id, const unsigned char speed, const float scale,
                 const jo_pos2D pos, const char health) {
  t_ship ship;

  ship.laser = jo_malloc(sizeof(t_weapon));
  ship.solid.health = health;
  ship.solid.speed = speed;
  ship.solid.sprite.pos.x = pos.x;
  ship.solid.sprite.pos.y = pos.y;
  ship.solid.sprite.pos.z = scale * 1000.0f;
  ship.solid.sprite.scale = scale;
  ship.solid.sprite.id = sprite_id;

  return ship;
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */
