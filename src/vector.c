// vector.c

#include "shooter/vector.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

extern inline vector2 vector2_init(const int x, const int y);
extern inline vector2_f vector2_f_init(const float x, const float y);

extern inline float vector2_length(const vector2 v);
extern inline float vector2_f_length(const vector2_f v);

extern inline vector2 vector2_sub(const vector2 v1, const vector2 v2);
extern inline vector2_f vector2_f_sub(const vector2_f v1, const vector2_f v2);

vector2 vector2_normalise(const vector2 v) {
  const float length = vector2_length(v);
  if (length > 0)
    return vector2_init(((float)v.x / length), ((float)v.y / length));

  return vector2_init(0, 0);
}

vector2_f vector2_f_normalise(const vector2_f v) {
  const float length = vector2_f_length(v);
  if (length > 0)
    return vector2_f_init(((float)v.x / length), ((float)v.y / length));

  return vector2_f_init(0.0f, 0.0f);
}
