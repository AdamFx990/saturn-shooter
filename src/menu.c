// menu.c

#include "shooter/menu.h"
#include "shooter/player.h"
#include "shooter/state.h"

#include <jo/jo.h>

/* ================ *
 * STATIC VARIABLES *
 * ================ */

static char menu_pos;
static char select_pressed;
static jo_font *menu_font;
static char *menu_items[] = {"BILLY-NO-MATES", "THE WIFE AND I"};
static jo_sound snd_menu_scroll;
static jo_sound snd_menu_select;

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static void menu_play_scroll_sound();

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void menu_init() {
  jo_audio_load_pcm("M_MOVE.PCM", JoSoundMono16Bit, &snd_menu_scroll);
  JO_ZERO(menu_pos);
  JO_ZERO(select_pressed);
  menu_font =
      jo_font_load(JO_ROOT_DIR, "FONT.TGA", JO_COLOR_Green, 8, 8, 2,
                   "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!\"?=%&',.()*+-/");
  array_init(&controller_ids, 1);

  for (char controller_id = 0; controller_id < JO_INPUT_MAX_DEVICE;
       controller_id++) {
    if (!jo_is_input_available(controller_id))
      continue;
    array_push(&controller_ids, controller_id);
  }
}

void menu_draw() {
  for (char i = 0; i < controller_ids.used; i++) {
    float font_scale;
    if (i == menu_pos) {
      jo_set_printf_color_index(JO_COLOR_Green);
      font_scale = 1.2f;
    } else {
      jo_set_printf_color_index(JO_COLOR_Blue);
      font_scale = 0.6f;
    }

    jo_font_print_centered(menu_font, 0,
                           ((menu_font->spacing * 1.1) *
                            (i - JO_DIV_BY_2(ARRAY_LENGTH(menu_items)))),
                           font_scale, menu_items[i]);
  }
}

void menu_input() {
  for (char controller_id = 0; controller_id < controller_ids.used;
       controller_id++) {
    if (jo_is_input_key_down(controller_id, JO_KEY_DOWN)) {
      menu_play_scroll_sound();
      menu_pos++;
    } else if (jo_is_input_key_down(controller_id, JO_KEY_UP)) {
      menu_play_scroll_sound();
      menu_pos--;
    } else if (jo_is_input_key_down(controller_id, JO_KEY_A) ||
               jo_is_input_key_down(controller_id, JO_KEY_START)) {
      player_init(&controller_ids, menu_pos + 1);
      select_pressed = 1;

      jo_audio_load_pcm("M_SEL.PCM", JoSoundMono16Bit, &snd_menu_select);
      jo_audio_play_sound(&snd_menu_select);
    }
  }
}

void menu_think() {
  if (select_pressed) {
    set_state(STATE_IN_GAME);
    select_pressed = 0;
    return;
  }
  if (menu_pos < 0)
    menu_pos = (ARRAY_LENGTH(menu_items) - 1);
  else if (menu_pos >= ARRAY_LENGTH(menu_items)) {
    JO_ZERO(menu_pos);
  }
}

void menu_dispose() {
  jo_audio_free_pcm(&snd_menu_scroll);
  jo_audio_free_pcm(&snd_menu_select);
  array_free(&controller_ids);
  jo_free(menu_font);
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */

static void menu_play_scroll_sound() {
  jo_audio_stop_sound(&snd_menu_scroll);
  jo_audio_play_sound(&snd_menu_scroll);
}
