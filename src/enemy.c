// enemy.c
#include "shooter/enemy.h"

#include "shooter/camera.h"
#include "shooter/point.h"
#include "shooter/projectile.h"
#include "shooter/weapon.h"

/* ================ *
 * STATIC VARIABLES *
 * ================ */

static unsigned char enemy_max;
static int enemy_sprite_id = -1;
static jo_list enemy_list;

/* ================= *
 * STATIC PROTOTYPES *
 * ================= */

static void enemy_move(jo_node *enemy_node);
static void enemy_respawn();
static void enemy_spawn();

/* ================ *
 * GLOBAL FUNCTIONS *
 * ================ */

void enemy_init() {
  enemy_max = 10;
  enemy_sprite_id = jo_sprite_add_tga(JO_ROOT_DIR, "ENEMY.TGA", JO_COLOR_Blue);
  jo_list_init(&enemy_list);
  enemy_list.allocation_behaviour = JO_MALLOC_TRY_REUSE_BLOCK;
  enemy_respawn();
}

void enemy_draw() {
  jo_list_foreach(&enemy_list, ship_draw_list);

#ifdef JO_DEBUG
#ifdef DEBUG_ENEMY
  jo_printf(18, 1, "enmy list count=%i", enemy_list.count);
#endif // DEBUG_ENEMY
#endif // JO_DEBUG
}

void enemy_think() {
  jo_list_foreach(&enemy_list, enemy_move);
  enemy_respawn();
}

jo_list* get_enemy_list() {
  return &enemy_list;
}

/* ================ *
 * STATIC FUNCTIONS *
 * ================ */

static void enemy_move(jo_node *enemy_node) {
  t_ship *const enemy = (t_ship *)enemy_node->data.ptr;

  enemy->solid.sprite.pos.y += enemy->solid.speed;

  if (point_outside_screen_bounds_bottom(enemy->solid.sprite.pos.y - 24)) {
    jo_list_remove(&enemy_list, enemy_node);
    jo_free(enemy);
    jo_free(enemy_node);
  }
}

static void enemy_respawn() {
  while (enemy_list.count < enemy_max) {
    enemy_spawn();
  }
}

static void enemy_spawn() {
  const t_camera* c = get_camera();
  t_ship *const enemy = jo_malloc(sizeof(t_ship));

  /* Initalise position a random distance
   * (up to 170 px) above the top of the screen. */
  enemy->solid.sprite.pos.x = (jo_random(JO_TV_WIDTH) - JO_TV_WIDTH_2) + c->pos.x;
  enemy->solid.sprite.pos.y = -(JO_TV_HEIGHT_2 + (16 + jo_random(170))) + c->pos.y;
  enemy->solid.sprite.pos.z = 520;

  const unsigned char speed = jo_random(3);
  const unsigned char base_dmg = 40;
  const unsigned char speed_mod = 20;
  const unsigned char dmg = base_dmg + (speed_mod * speed);
  enemy->solid.sprite.id = enemy_sprite_id;
  enemy->solid.sprite.scale = 0.5f;
  enemy->solid.speed = speed;
  enemy->solid.health = 20;
  enemy->solid.reward = 50;
  enemy->solid.dmg = dmg;

  jo_list_data data = {.ptr = enemy};
  jo_list_add(&enemy_list, data);
}
